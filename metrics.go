package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

func defineMetrics() {
	avgResTime = promauto.NewGauge(prometheus.GaugeOpts{
		Name:        "btp_perf_mon",
		Help:        "Average performance of BTP response time in ms",
		ConstLabels: prometheus.Labels{"destination": label, "instance": hostname},
	})

	sentRequestsMetric = promauto.NewCounter(prometheus.CounterOpts{
		Name:        "btp_perf_sent_requests",
		Help:        "Sent requests to target url",
		ConstLabels: prometheus.Labels{"destination": label, "instance": hostname},
	})

	receivedRequestsMetric = promauto.NewCounter(prometheus.CounterOpts{
		Name:        "btp_perf_received_requests",
		Help:        "Received requests to target url",
		ConstLabels: prometheus.Labels{"destination": label, "instance": hostname},
	})
}

package main

import (
	"os"
	"strconv"

	"github.com/rs/zerolog/log"
)

func envWarning(env string) {
	log.Fatal().Msgf("environment variable %s is missing", env)
}

func envCheck(variable string) bool {
	log.Debug().Str("checking env variable ", variable).Msg("")
	_, envSet := os.LookupEnv(variable)
	if !envSet {
		log.Debug().Str("env variable ", variable).Msg("not set")
		envWarning(variable)
	}
	return envSet
}

func envSetInt(envName string, variable *int) {
	log.Debug().Str("setting env variable (integer) ", envName).Msg("")
	env := os.Getenv(envName)
	log.Debug().Str("got value back: ", env).Msg("")
	if env == "" {
		envWarning(envName)
	}
	envInt, err := strconv.Atoi(env)
	if err != nil {
		envWarning(envName)
	}
	*variable = envInt
}

// work with a reference of variable to allow modifing the "original" variable
func envSet(envName string, variable *string) {
	log.Debug().Str("setting env variable (string) ", envName).Msg("")
	env := os.Getenv(envName)
	log.Debug().Str("got value back: ", env).Msg("")
	if env == "" {
		envWarning(envName)
	}
	*variable = env
}

func envReady() bool {
	log.Debug().Msg("checking env variables")
	if envCheck("PORT") && envCheck("URL") && envCheck("LABEL") {
		envSetInt("PORT", &port)
		// passing a reference of url to the envSet function
		envSet("URL", &url)
		envSet("LABEL", &label)
		return true
	} else {
		return false
	}
}

func flightReady() bool {
	log.Debug().Msg("performing preflight check now")
	return envReady()
}

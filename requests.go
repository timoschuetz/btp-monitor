package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
)

func sendRequests() {
	go func() {
		log.Info().Msg("starting scheduled requests")
		for {
			inhalt := Data{
				Time: time.Now().UnixMilli(),
				Test: "Test1234",
			}

			body, _ := json.Marshal(inhalt)

			log.Debug().Str("sending request to ", url).Msg("")
			resp, err := http.Post(url, "application/json", bytes.NewBuffer(body))
			// Increase counter
			sentRequestsMetric.Inc()

			if err != nil {
				log.Error().Err(err).Msg("error while sending request")
			}

			if resp != nil {
				log.Debug().Msg("received a valid response")
			}

			// Sleep timer
			log.Debug().Msg("going to sleep for 5 seconds")
			time.Sleep(5 * time.Second)
		}
	}()
}

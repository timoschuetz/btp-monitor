# BUILD STAGE
FROM golang as build
WORKDIR /work
ADD . .
RUN go test .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o btp_monitor .

# EXECUTE STAGE
FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=build /work/btp_monitor .
CMD ["./btp_monitor"]

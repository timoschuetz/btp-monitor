package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var (
	avgResTime             prometheus.Gauge
	sentRequestsMetric     prometheus.Counter
	receivedRequestsMetric prometheus.Counter
	url                    string
	label                  string
	hostname               string
	port                   int
)

type Data struct {
	Time int64
	Test string
}

func main() {
	// Logger setup
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	debugEnv, debugEnvValue := os.LookupEnv("DEBUG")
	if debugEnvValue && debugEnv == "ON" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}

	if flightReady() {
		hostnameNew, err := os.Hostname()
		if err != nil {
			log.Warn().Msg("hostname not set correctly, will fall back to default")
			hostname = "BTP MONITOR DEFAULT HOST"
		} else {
			hostname = hostnameNew
		}

		defineMetrics()

		sendRequests()
		http.Handle("/metrics", promhttp.Handler())
		http.HandleFunc("/test", test)

		log.Info().Msg("starting HTTP server")
		serverURL := fmt.Sprintf(":%d", port)
		log.Info().Str("listening on ", serverURL).Msg("")
		http.ListenAndServe(serverURL, nil)

	}

}

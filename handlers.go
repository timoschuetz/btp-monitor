package main

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/rs/zerolog/log"
)

// /test handler
func test(w http.ResponseWriter, r *http.Request) {

	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Debug().Err(err).Msg("request was not correctly, will not accept this request")
		w.WriteHeader(400)
	}

	var data Data

	json.Unmarshal(body, &data)

	// Check if time is not null
	if data.Time <= 0 {
		w.WriteHeader(400)
	}

	var timeDiff = time.Now().UnixMilli() - data.Time
	log.Debug().Str("time difference", strconv.FormatInt(int64(timeDiff), 10)).Msg("")

	if timeDiff == 0 {
		log.Debug().Msg("time difference 0 detected")
	}

	receivedRequestsMetric.Inc()

	avgResTime.Set(float64(timeDiff))

	w.WriteHeader(200)
}
